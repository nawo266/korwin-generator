import sys
#from PyQt5 import QtCore, QtGui, QtWidgets
from PySide2 import QtCore, QtGui, QtWidgets
import random

class Ui_Generator(object):

    def setupUi(self, Generator):
        Generator.setObjectName("Generator")
        Generator.resize(524, 212)
        self.gridLayout = QtWidgets.QGridLayout(Generator)
        self.gridLayout.setObjectName("gridLayout")
        self.pushButton = QtWidgets.QPushButton(Generator)
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.randomText)
        self.gridLayout.addWidget(self.pushButton, 0, 0, 1, 1)
        self.plainTextEdit = QtWidgets.QPlainTextEdit(Generator)
        self.plainTextEdit.setEnabled(True)
        self.plainTextEdit.setUndoRedoEnabled(False)
        self.plainTextEdit.setReadOnly(True)
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.gridLayout.addWidget(self.plainTextEdit, 1, 0, 1, 1)

        self.retranslateUi(Generator)
        QtCore.QMetaObject.connectSlotsByName(Generator)

    def retranslateUi(self, Generator):
        _translate = QtCore.QCoreApplication.translate
        Generator.setWindowTitle(_translate("Generator", "Generator wypowiedzi Janusza Korwin-Mikkego v0.1"))
        self.pushButton.setText(_translate("Generator", "Losuj"))
        #self.setWindowIcon(QtGui.QIcon("korwin.jpg"))
        Generator.setWindowIcon(QtGui.QIcon("korwin.png"))



    def randomText(self):
        ##wypowiedzi w tablicach:
        txt1 = ["Proszę zwrócić uwagę, że ","I tak mam trzy razy mniej czasu, więc proszę mi pozwolić powiedzieć: ", "Państwo się się śmieją, ale ", "Ja nie potrzebowałem edukacji seksualnej, żeby wiedzieć, że ", "no niestety: ", "Gdzie leży przyczyna problemu? Ja państwu powiem: ","Państwo chyba nie wiedzą, że ", "Oświadczam kategorycznie: ", "Powtarzam: ", "Powiedzmy to z całą mocą: ", "W Polsce dzisiaj ","Państwo sobie nie zdają sprawy, że ","To ja przepraszam bardzo: ", "Otóż nie wiem, czy Pan wie, że ","Yyyyy... ","Ja chcę powiedzieć jedną rzecz: ","Trzeba powiedzieć jasno: ", "Jak powiedział wybitny krakowianin Stanisław Lem, ","Proszę mnie dobrze zrozumieć: ","Ja chciałem państwu przypomnieć, że ","Niech państwo nie mają złudzeń: ","Powiedzmy to wyraźnie: "]
        txt2 = ["właściciele niewolników ", "związkowcy ", "trockiści ", "tak zwane dzieci kwiaty ","rozmaici urzędnicy ","federaści ","etatyści ","ci durnie i złodzieje ","ludzie wybrani głosami meneli spod budki z piwem ","socjaliści pobożni ", "socjaliści bezbożni ", "komuniści z krzyżem w zębach ", "agenci obcych służb ","członkowie bandy czworga ","pseudo-masoni z Wielkiego Wschodu Francji ", "przedstwaiciele czerwonej hołoty ","ci wszyscy (tfu!) geje ","funkcjonariusze reżymowej telewizji ","tak zwani ekolodzy ","ci wszyscy (tfu!) demokraci ", "agenci bezpieki ","feminazistki "]
        txt3 = ["po przeczytaniu Manifestu komunistycznego ","którymi się brzydzę ","których nienawidzę ","z okolic Gazety Wyborczej ","czyli taka żydokomuna ","odkąd zniesiono karę śmierci ","którymi pogardzam ","których miejsce w normalnym kraju jest w więzieniu ","na polecenie Brukseli ","posłusznie ","bezmyślnie ","z nieprawdopodobną pogardą dla człowieka ","za pieniądze podatników ","zgodnie z ideologią LGBTQ ","za wszelką cenę ","zupełnie bezkarnie ","całkowicie bezczelnie ","o poglądach na lewo od komunizmu ","celowo i świadomie ","z premedytacją ","od czasów Okrągłego Stołu ", "w ramach postępu "]
        txt4 = ["udają homoseksualistów ","niszczą rodzinę ","idą do polityki ","zakazują góralom robienia oscypków ","organiuzują paraolimpiady ","wprowadzają ustrój, w którym raz na cztery lata można wybrać sobie pana ","ustawiają fotoradary ","wprowadzają dotacje ","wydzielają buspasy ","podnoszą wiek emerytalny ","rżną głupa ","odbierają dzieci rodzicom ","wprowadzają absurdalne przepisy ","umieszczają dzieci w szkołach koedukacyjnych ","wprowadzają parytety ","nawołują do podniesienia podatków ","próbują wyrzucić kierowców z miast ","próbują skłócić Polskę z Rosją ","głoszą brednie o globalnym ociepleniu ","zakazują posiadania broni ","nie dopuszczają prawicy do władzy ","uczą dzieci homoseksualizmu "]
        txt5 = ["żeby poddawać wszystkich tresurze ","bo taka jest ich natura ","bo chcą wszystko kontrolować ","bo nie rozumieją, że socjalizm nie działa ","żeby wreszcie zapanował socjalizm ","dokładnie tak, jak tow. Janosik ","zamiast pozwolić ludziom zarabiać ","żeby wyrwać kobiety z domu ","bo to jest w interesie tak zwanych ludzi pracy ","zamiast pozwolić decydować konsumentowi ","żeby nie opłacało się mieć dzieci ","zamiast obniżyć podatki ","bo nie rozumieją, że selekcja naturalna jest czymś dobrym ","żeby mężczyźni przestali być agresywni ","bo dzięki temu mogą brać łapówki ","bo dzięki temu mogą kraść ","bo dostają za to pieniądze ","bo tak się uczy w państwowej szkole ","bo bez tego (tfu!) demokracja nie może istnieć ","bo głupich jest więcej niż mądrych ","bo chcą tworzyć raj na ziemi ","bo chcą niszczyć cywilizację białego człowieka "]
        txt6 = ["co ma zresztą tyle samo sensu, co zawody w szachach dla debili.","co zostało dokładnie zaplanowane w Magdalence przez śp. generała Kiszczaka.","i trzeba być idiotą żeby ten system popierać.","ale nawet ja jeszcze dożyję normalnych czasów.","co dowodzi, że wyskrobano nie tych, co trzeba.","a zwykłym ludziom wmawiają, że im coś dadzą.","- cóż: chcieliście (tfu!) demokracji, to macie.","dlatego trzeba zlikwidować koryto, a nie zmieniać świnie.","a wystarczyłoby przestać wypłacać zasiłki.","podczas gdy normalni ludzie uważani są za dziwaków.","co w wieku XIX po prostu by wyśmiano.","- dlatego w społeczeństwie jest równość, a powinno być rozwarstwienie.","co prowadzi Polskę do katastrofy.","- dlatego trzeba przywrócić normalność.","ale w wolnej Polsce pójdą siedzieć.","przez kolejne kadencje.","o czym się nie mówi.","i właśnie dlatego Europa umiera.","ale przyjdą muzułmanie i zrobią porządek.","- tak samo zresztą jak za Hitlera.","- proszę zobaczyć, co się dzieje na zachodzie, jeśli mi państwo nie wierzą.","co lat temu sto nikomu nie przyszłoby nawet do głowy"]
        self.plainTextEdit.clear() #czyszczenie tekstu
        ##losowanie tekstu i wstawianie ich w zmienne. Wstawianie w listę na mnie krzyczało ;_;
        randomTxt1 = random.sample(txt1,1)
        randomTxt2 = random.sample(txt2,1)
        randomTxt3 = random.sample(txt3,1)
        randomTxt4 = random.sample(txt4,1)
        randomTxt5 = random.sample(txt5,1)
        randomTxt6 = random.sample(txt6,1)
        #Wstawianie tekstu w pole tekstowe GUI:
        self.plainTextEdit.insertPlainText(randomTxt1[0])
        self.plainTextEdit.insertPlainText(randomTxt2[0])
        self.plainTextEdit.insertPlainText(randomTxt3[0])
        self.plainTextEdit.insertPlainText(randomTxt4[0])
        self.plainTextEdit.insertPlainText(randomTxt5[0])
        self.plainTextEdit.insertPlainText(randomTxt6[0])




if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    Generator = QtWidgets.QDialog()
    ui = Ui_Generator()
    ui.setupUi(Generator)
    Generator.show()
    sys.exit(app.exec_())
